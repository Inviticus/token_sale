
#!/usr/bin/env bash
solidity_flattener --solc-paths=zeppelin-solidity=$(pwd)/node_modules/zeppelin-solidity/ contracts/Presale.sol --out flat_Presale.sol
solidity_flattener --solc-paths=zeppelin-solidity=$(pwd)/node_modules/zeppelin-solidity/ contracts/TokenSale.sol --out flat_Sale.sol
solidity_flattener --solc-paths=zeppelin-solidity=$(pwd)/node_modules/zeppelin-solidity/ contracts/TKN.sol --out flat_TKN.sol
